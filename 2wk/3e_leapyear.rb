# Write a method leap_year?. It should accept a year value from the user, check whether it's a leap year, and then return true or false. With the help of this leap_year?() method calculate and display the number of minutes in a leap year (2000 and 2004) and the number of minutes in a non-leap year (1900 and 2005). Note: Every year whose number is divisible by four without a remainder is a leap year, excepting the full centuries, which, to be leap years, must be divisible by 400 without a remainder. If not so divisible they are common years. 1900, therefore, is not a leap year.

#rubydoctest: leap_year?"1600"
#>> leap_year?(1600)
#=> true 

#rubydoctest: leap_year?"2016"
#>> leap_year?(2016)
#=> true
#
#rubydoctest: leap_year?"1900"
#>> leap_year?(1900)
#=> false
#
#rubydoctest: leap_year? "2005"
#>> leap_year?(2005)
#=> false
#
def leap_year?(year)
  if year % 4 == 0
    @bool = true
    if year % 100 == 0 
      @bool = false
      if year % 400 == 0
        @bool = true
      end
    end
  end
  @bool
end
#rubydoctest : minutes_in_year
#>> minutes_in_year 1900
#=> 525600
#
#rubydoctest : minutes_in_year
#>> minutes_in_year 2016
#=> 527040
#
def minutes_in_year(year)
  if leap_year? year
    366 * 24 * 60
  else
    365 * 24 * 60
  end
end


