def change_file(filename)
 file = File.open("./#{filename}", "r+")
  text_string = file.read
  change_text_index = text_string.index("word")
  text_string.insert(change_text_index,"inserted ")
  file.rewind
  file.write text_string
  file.close 
end

def reset(filename)
  File.open(filename,'w') do |file|
    file.puts 'text text text text text
text text text text text
text text word text text
text text text text text
text text text text text'
  end
end

if __FILE__ == $0
  puts '>> would you like to (c)hange or(r)eset the file?'
  puts 'insert c or r >>'; opt = gets.chomp
  if opt == 'c' 
    change_file('./2e_insert_word_dat.txt')
  else
    reset('./2e_insert_word_dat.txt')
  end
end
