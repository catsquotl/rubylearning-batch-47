According to the Kernel.sprintf documentation.
puts "%05d" % 123

A format sequence consists of a percent sign,
followed by optional flags, width, and precision indicators,
then terminated with a field type character.

A format String sequence starts with %. 
Then the flag zero which acording to the docs pads extra width with zero`s
Then the total width. 5 in this case and finaly a field type character.
In this case d which stands for a conversion of the argument (123) as a decimal number..
So we get a string 00123. 5 digits wide padded with zero`s

Looking at the String#% documentation we learn that calling a percent sign on a string of formatting symbols will use the argument given to that string is converted to a string with the specified format.

The way the above line of code is written uses some convenient way of writing it. more codelike would be...
puts "%05d".%(123) where we call % on a string literal with the dot operator and give it the argument 123.
