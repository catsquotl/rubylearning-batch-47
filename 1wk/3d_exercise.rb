  @time_values_in_seconds = Hash.new
  @time_values_in_seconds[:seconds] = 1
  @time_values_in_seconds[:minutes] = 60  * @time_values_in_seconds[:seconds]
  @time_values_in_seconds[:hour]    = 60  * @time_values_in_seconds[:minutes]
  @time_values_in_seconds[:day]     = 24  * @time_values_in_seconds[:hour]
  @time_values_in_seconds[:year]    = 365 * @time_values_in_seconds[:day]

def time_values_in_seconds(sym)
  @time_values_in_seconds[sym]
end

def convert_age(age)
  age.to_f / time_values_in_seconds(:year)
end

def format_age(age, precision = 2)
  format("%.#{precision}f",convert_age(age))
end

if __FILE__ == $0
  age_in_seconds = 9.79e8
  puts format_age(age_in_seconds)
end
