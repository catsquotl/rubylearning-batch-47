  @time_values_in_seconds = Hash.new
  @time_values_in_seconds[:Second] = 1.0
  @time_values_in_seconds[:minute] = 60  * @time_values_in_seconds[:Second]
  @time_values_in_seconds[:hour]    = 60  * @time_values_in_seconds[:minute]
  @time_values_in_seconds[:day]     = 24  * @time_values_in_seconds[:hour]
  @time_values_in_seconds[:year]    = 365 * @time_values_in_seconds[:day]

def time_values(sym)
  @time_values_in_seconds[sym]
end

# doctest: times_converter( from, to ) examples
# >> times_converter(:Second, :Second)
# => 1
# doctest: from minutes to seconds
# >> times_converter(:minute, :Second)
# => 60
# doctest: from hours to seconds
# >> times_converter(:hour, :Second)
# => 3600
def times_converter(from,to)
  conversion = time_values(from) / time_values(to)
  conversion % 1 == 0 ? conversion.to_i : conversion
end

if __FILE__ == $0
  puts "No running code at the moment... see the IRB comment examples of how"
  puts "to use this program!"
end


