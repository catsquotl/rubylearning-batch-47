#doctest: convert fahrentheit to celsius
#>> convert 100
#=> 37.78

def convert(fahrenheit)
  format("%.2f",(fahrenheit - 32) *  (5.0 / 9)).to_f
end
