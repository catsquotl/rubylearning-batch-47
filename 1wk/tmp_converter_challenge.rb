
dtef convert(from,to,degrees)
  #nil
end

#doctest: convert fahrentheit to celsius
#>> f_to_c(100)
#=> 37.78

def f_to_c(degrees)
 ((degrees - 32) * (5.0 / 9)).round(2)
end

#doctest: Celsius to Fahrenheit
#>> c_to_f 100
#=> 212

def c_to_f(degrees)
  (degrees * 9 / 5) + 32
end

if __FILE__ == $0
 p f_to_c 100
end
